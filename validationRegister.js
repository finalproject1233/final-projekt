var 	fname = document.getElementById("fname"),
		lname = document.getElementById("lname"),
		uname = document.getElementById("uname"),
		pass = document.getElementById("pass"),
		repass = document.getElementById("repass"),
		formlogin = document.getElementById('formLogin'),
		btn9 = document.getElementById("btn9"),
		x = /[a-zA-Z]{2,}/,   //regEx for names
		y = /[a-zA-Z0-9]{5,}/; // regEx for password

	formlogin.addEventListener('submit', validate);

	/***
	 * Validates all fields in the Register form
	 * returns {Bool}
	 */
	function validate (evnt) {
		console.log(evnt);
		evnt.preventDefault();
		
		if(Val_name(fname,x) && Val_name(lname,x) && Val_name(uname,y) && Val_name(pass,y) && Val_SecPass()) {

			return true;
		}else {
			return false;
		}
	}	

	// validate first name, last name, user name and first password

	function Val_name(element , pattern) {

		var value = element.value,
			name = element.name;

		if (value == "" || value == null) {

			element.style.border = "2px solid red";
			console.log('Please input ' + name + '  name');
			element.focus();
			return false;
		}
		else if (pattern.test(value) == false) {

			console.log("Plese enter correct" + name + " name");
			element.focus();
			return false;
		}

		return true;
		element.style.border = "0px";
	}

	// validate second password

	function Val_SecPass()	{
		var vpass = pass.value,
			vrepass = repass.value;
		if (vpass != vrepass)
		{
			console.log("Please repeat the password correctly");
			repass.focus();
			return false;
		}
	}

/***
	 * end of Validates Register form
	 */

/***
 *** Check for existing username and password in login form
**/

	var user1 = {name: "abc123" , passw: "abc123"},
		user2 = {name: "abc124" , passw: "abc124"},
		user3 = {name: "abc125" , passw: "abc125"};

	function check(){

		function checkPass(pass){

			if (pass.value == "abc123" || pass.value == "abc124" || pass.value == "abc125"){
					console.log("Sucsess login")}

				else {("Wrong password");}
		}

		switch(uname.value){
			case "abc123": checkPass(pass);
			case user2.name: checkPass(pass);
			case user3.name: checkPass(pass); 
			default: console.log("Wrong username"); break;	
		}			
	}

/***
 *** end of check for existing username and password in login form
**/


/**** going to login form and back to register form on same html document
***/

var logLink = document.getElementById("logLink"),  // this is the link to login form
	h1 = document.getElementById("h1"),            
	span = document.getElementsByTagName("span");
	
	logLink.addEventListener("click", login); // add event to a link

function login(){
	fname.style.display = "none";
	lname.style.display = "none";
	repass.style.display = "none";
	h1.innerHTML = "login";
	btn9.style.value = "LOGIN",
	logLink.innerHTML = "Not a member? Please register here!";
	for (var i=0 ; i<span.length ; i++){
		span[i].style.display = "none";
	}	

	logLink.removeEventListener("click", login);
	logLink.addEventListener("click", register);

	formlogin.removeEventListener('submit', validate);
	formlogin.addEventListener('submit', check);
}

function register(){

	fname.style.display = "block";
	lname.style.display = "block";
	repass.style.display = "block";
	h1.innerHTML = "register";
	logLink.innerHTML = "Already a member? Please, log in here!";
	for (var i=0 ; i<span.length ; i++){
		span[i].style.display = "block";
	}	

	logLink.removeEventListener("click" , register);
	logLink.addEventListener("click" , login);

	formlogin.removeEventListener('submit', check);
	formlogin.addEventListener('submit', validate);
}


/**** end of the changе of forms
***/

